import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateObject, format } from '../../../shared/utility';
import { Redirect} from 'react-router-dom';
import { isEqual } from 'lodash'; 

import classes from './Person.css';
import gridSystem from '../../../shared/gridSystem.css';
import Input from '../../../components/Input/Input';
import Select from '../../../components/Select/Select';
import * as actions from '../../../store/actions/index';
import Button from '../../../components/Button/Button';

class Person extends Component {
    
    state = {
        person : {
            id: {
                value: 0
            },
            name : {
                value: "" 
            },
            dateBirth: {
                value: ""
            },
            identityDocument:{
                value: "",
                pattern: "###.###.###-##"
            },
            email: {
                value: ""
            },
            street: {
                value: ""
            },
            addressNumber:{
                value: ""
            },
            complement:{
                value: ""
            },
            zipCode:{
                value: "",
                pattern: "#####-###"
            },
            neighborhood:{
                value: ""
            },
            city: {
                value: ""
            },
            province: {
                value: ""
            }
        }
    }

    onBlurHandler = (event,inputIdentifier) => {
        event.preventDefault();
        const updatedFormElement = updateObject(this.state.person[inputIdentifier],{
            value: format(event.target.value,this.state.person[inputIdentifier].pattern),
        })

        const updatedOrderForm = updateObject(this.state.person,{
            [inputIdentifier]:updatedFormElement
        })
        this.setState({person: updatedOrderForm});
    }

    onFocusHandler = (event,inputIdentifier) => {
        event.preventDefault();
        const updatedFormElement = updateObject(this.state.person[inputIdentifier],{
            value: "",
        })

        const updatedOrderForm = updateObject(this.state.person,{
            [inputIdentifier]:updatedFormElement
        })
        this.setState({person: updatedOrderForm});
    }

    componentDidMount() {
        this.props.onLoadProvinces();
        if (this.props.match.params.id !== 0) {
            const idPerson = this.props.match.params.id;
            this.props.onLoadPerson(idPerson);
            if (this.props.person) {
                this.mapPersonToForm(this.props.person);
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (!isEqual(nextProps.person,this.state.person)){
            this.mapPersonToForm(nextProps.person);
        }
    }

    mapPersonToForm(person) {
        if (person) {
            const newPerson = {};
            for (let field in person) {
                if (person[field] && this.state.person[field]) {
                    newPerson[field] = {
                        value: format(person[field] ? person[field] : "",
                            this.state.person[field] ? this.state.person[field].pattern : "")
                    }

                }
            }
            for (let field in person.address) {
                newPerson[field] = {
                    value: format(person.address[field] ? person.address[field] : "",
                        this.state.person[field] ? this.state.person[field].pattern : "")
                }
            }
            this.setState({
                person: newPerson
            });
        }
    }

    inputChangeHandler = (event,inputIdentifier) => {
        event.preventDefault();
        const updatedFormElement = updateObject(this.state.person[inputIdentifier],{
            value: event.target.value,
        })

        const updatedOrderForm = updateObject(this.state.person,{
            [inputIdentifier]:updatedFormElement
        })
        this.setState({person: updatedOrderForm});
    }

    onSaveHandler = (event) => {
        event.preventDefault();
        const personData = {
            id : this.state.person.id.value,
            name: this.state.person.name.value,
            dateBirth: this.state.person.dateBirth.value,
            identityDocument:this.state.person.identityDocument.value,
            email:this.state.person.email.value,
            address: {
                city: this.state.person.city.value,
                neighborhood:this.state.person.neighborhood.value,
                complement: this.state.person.complement.value,
                street:this.state.person.street.value,
                zipCode:this.state.person.zipCode.value,
                addressNumber:this.state.person.addressNumber.value,
                province: this.state.person.province.value,
            }
        }
        this.props.onSavePerson(personData);
    }
    onCancelHandler = () => {
        this.props.history.push("/")
    }

    render(){

        let redirect;
        if (this.props.saved){
            redirect = <Redirect to="/" />
        }
        return(
            <div className={classes.Person}>
            {redirect}
            <form onSubmit={this.onSaveHandler}>
                <div className={classes.Title}>
                    <h3>Person</h3>
                </div>
                <div className={classes.Body}>
                    <div className={gridSystem.Row}>
                        <div className={gridSystem.Col_12}>
                            <label>Name</label>
                            <Input onChange={(event) => this.inputChangeHandler(event,'name')}
                                value={this.state.person.name.value} placeholder="Your name"/>
                        </div>
                     </div>
                    <div className={gridSystem.Row}>
                        <div className={gridSystem.Col_3}>
                            <label>Date of Birth</label>
                            <Input type="date" onChange={(event) => this.inputChangeHandler(event,'dateBirth')}
                                value={this.state.person.dateBirth.value} placeholder="Your Date of birth"/>
                        </div>
                        <div className={gridSystem.Col_3}>
                            <label>CPF</label>
                            <Input onFocus={(event) => this.onFocusHandler(event,'identityDocument')}
                                onBlur={(event) => this.onBlurHandler(event,'identityDocument')} 
                                onChange={(event) => this.inputChangeHandler(event,'identityDocument')} 
                                value={this.state.person.identityDocument.value} placeholder="Your Social Security"/>
                        </div>
                        <div className={gridSystem.Col_6}>
                            <label>E-mail</label>
                            <Input  type="email" onChange={(event) => this.inputChangeHandler(event,'email')} 
                                value={this.state.person.email.value} placeholder="Your E-mail"/>
                        </div>
                    </div>
                    <div className={gridSystem.Row}>
                        <div className={gridSystem.Col_12}>
                            <div className={classes.Address}>
                                <div className={gridSystem.Row}>
                                    <div className={gridSystem.Col_9}>
                                        <label>Street</label>
                                        <Input onChange={(event) => this.inputChangeHandler(event,'street')} 
                                            value={this.state.person.street.value} placeholder="Your Street" />
                                    </div>
                                    <div className={gridSystem.Col_3}>
                                        <label>Number</label>
                                        <Input onChange={(event) => this.inputChangeHandler(event,'addressNumber')} 
                                            value={this.state.person.addressNumber.value} placeholder="Your Address Number" />
                                    </div>
                                </div>
                                <div className={gridSystem.Row}>
                                    <div className={gridSystem.Col_9}>
                                        <label>Complement</label>
                                        <Input onChange={(event) => this.inputChangeHandler(event,'complement')} 
                                            value={this.state.person.complement.value} placeholder="Your complement" />
                                    </div>
                                    <div className={gridSystem.Col_3}>
                                        <label>ZipCode</label>
                                        <Input 
                                            onFocus={(event) => this.onFocusHandler(event,'zipCode')}
                                            onBlur={(event) => this.onBlurHandler(event,'zipCode')} 
                                            onChange={(event) => this.inputChangeHandler(event,'zipCode')} 
                                            value={this.state.person.zipCode.value} placeholder="Your ZipCode" />
                            </div>
                        </div>
                        <div className={gridSystem.Row}>
                            <div className={gridSystem.Col_4}>
                                <label>Neighborhood</label>
                                <Input onChange={(event) => this.inputChangeHandler(event,'neighborhood')} 
                                    value={this.state.person.neighborhood.value} placeholder="Your Neighborhood" />
                            </div>
                            <div className={gridSystem.Col_4}>
                                <label>City</label>
                                <Input onChange={(event) => this.inputChangeHandler(event,'city')}
                                    value={this.state.person.city.value} placeholder="Your City" />
                            </div>
                            <div className={gridSystem.Col_4}>
                                <label>Province</label>
                                <Select onChange={(event) => this.inputChangeHandler(event,'province')} 
                                    select={this.state.person.province.value} fields={this.props.provinces} />
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
                <div className={classes.Footer}>
                    <Button type="primary">Save</Button>
                    <Button onClick={this.onCancelHandler} 
                        type="danger" position="right">Cancel</Button>
                </div>
                </form>
            </div>
            
        )
    }
}

const mapStateToProps = state => {
    return {
        provinces: state.resource.provinces,
        saved: state.person.saved,
        person: state.person.current
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSavePerson: (personData) => dispatch(actions.savePerson(personData)),
        onLoadProvinces: () => dispatch(actions.loadProvinces()),
        onLoadPerson: (idPerson) => dispatch(actions.getPerson(idPerson))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Person);