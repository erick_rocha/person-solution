import React, { Component } from 'react';
import { withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

import classes from './Persons.css';
import { updateObject, format } from '../../shared/utility';
import gridSystem from '../../shared/gridSystem.css';
import { Link } from 'react-router-dom';
import Input from '../../components/Input/Input';
import Button from '../../components/Button/Button';
import Pagination from '../../components/Pagination/Pagination';
import FormatedCell from '../../components/FormatedCell/FormatedCell';

class Persons extends Component {

    state = {
        searchForm : {
            name : {
               value: ""     
            },
            identityDocument : {
                value: "",
                pattern: "###.###.###-##"     
             },
            email : {
                value: ""     
            },
            dateBirth : {
                value: ""     
            }, 
        },
        page: 0,
        perPage: 5,
        direction: 'ASC',
        sortBy:"id"
    }

    inputChangeHandler = (event,inputIdentifier) => {
        event.preventDefault();
        const updatedFormElement = updateObject(this.state.searchForm[inputIdentifier],{
            value: event.target.value,
        })

        const updatedOrderForm = updateObject(this.state.searchForm,{
            [inputIdentifier]:updatedFormElement
        })
        this.setState({searchForm: updatedOrderForm});
    }

    handlerSearch = (pPage,pPerPage) => {
        const name = this.state.searchForm.name.value;
        const identityDocument = this.state.searchForm.identityDocument.value;
        const email = this.state.searchForm.email.value;
        const dateBirth = this.state.searchForm.dateBirth.value;
        const page = pPage ? pPage : this.state.page;
        const perPage = pPerPage ? pPerPage : this.state.perPage; 
        this.props.onLoadPersons(name, identityDocument, email, dateBirth,
            page, perPage, this.state.direction, this.state.sortBy);

    }

    handlerPagination = (event,page) => {
        event.preventDefault();
        this.handlerSearch(page);
    }

    handlerPerPageSelect = (event) => {
        event.preventDefault();
        this.setState({
            searchForm: this.state.searchForm,
            perPage: event.target.value,
            page: this.state.page
        })
        this.handlerSearch(null,event.target.value);
    }

    componentDidMount(){
        this.handlerSearch();
    }

    onBlurHandler = (event,inputIdentifier) => {
        event.preventDefault();
        const updatedFormElement = updateObject(this.state.searchForm[inputIdentifier],{
            value: format(event.target.value,this.state.searchForm[inputIdentifier].pattern),
        })

        const updatedOrderForm = updateObject(this.state.searchForm,{
            [inputIdentifier]:updatedFormElement
        })
        this.setState({searchForm: updatedOrderForm});
    }

    onFocusHandler = (event,inputIdentifier) => {
        event.preventDefault();
        const updatedFormElement = updateObject(this.state.searchForm[inputIdentifier],{
            value: "",
        })

        const updatedOrderForm = updateObject(this.state.searchForm,{
            [inputIdentifier]:updatedFormElement
        })
        this.setState({searchForm: updatedOrderForm});
    }

    handlerNew = () => {
        this.props.history.push("/person/0");
    }

    render() {
        return(
            <div className={classes.Persons}>
                <div className={classes.Header}>
                    <h3>Persons</h3>
                </div>

                <Button type="primary"  onClick={this.handlerNew}>New</Button>
                
                <div className={classes.SearchBox}>
                    <div className={gridSystem.Row}>
                                <div className={gridSystem.Col_3}>
                                    <label>Name</label>
                                    <Input onChange={(event) => this.inputChangeHandler(event,'name')}
                                        value={this.state.searchForm.name.value} placeholder="Name"/>
                                </div>
                                <div className={gridSystem.Col_3}>
                                    <label>Identity Document</label>
                                    <Input onFocus={(event) => this.onFocusHandler(event,'identityDocument')}
                                        onBlur={(event) => this.onBlurHandler(event,'identityDocument')} 
                                        onChange={(event) => this.inputChangeHandler(event,'identityDocument')}
                                        value={this.state.searchForm.identityDocument.value} placeholder="Identity Document"/>
                                </div>
                                <div className={gridSystem.Col_3}>
                                    <label>E-mail</label>
                                    <Input onChange={(event) => this.inputChangeHandler(event,'email')} 
                                        value={this.state.searchForm.email.value} placeholder="E-mail"/>
                                </div>
                                <div className={gridSystem.Col_3}>
                                    <label>Birth of date</label>
                                    <Input type="date" onChange={(event) => this.inputChangeHandler(event,'dateBirth')}
                                        value={this.state.searchForm.dateBirth.value} placeholder="Birth of date"/>
                                </div>
                                <div className={gridSystem.Col_3}>
                                    <button onClick={this.handlerSearch} className={classes.ButtonSearch}>Search</button>
                                </div>
                    </div>
                </div>
                <div className={classes.TableBox}>
                <table className={classes.Table}>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th className={classes.MinSize_85}>Date Birth</th>
                        <th className={classes.MinSize_130}>Name</th>
                        <th>CPF</th>
                        <th>E-mail</th>
                        <th className={classes.MinSize_130}>Street</th>
                        <th>Address Nº</th>
                        <th>Complement</th>
                        <th>ZipCode</th>
                        <th>Neighborhood</th>
                        <th>City</th>
                        <th>Province</th>
                        <th className={classes.center}>Edit</th>
                        <th className={classes.center}>Delete</th>
                    </tr>
                </thead>
                <tbody>
                {
                    this.props.persons.map(person => (
                        <tr key={person.id}>
                            <td className={classes.Center}>{person.id}</td>
                            <td>{person.dateBirth}</td>
                            <td>{person.name}</td>
                            <td><FormatedCell value={person.identityDocument} pattern="###.###.###-##" /></td>
                            <td>{person.email}</td>
                            <td>{person.address.street}</td>                           
                            <td className={classes.Center}>{person.address.addressNumber}</td>
                            <td>{person.address.complement}</td>
                            <td><FormatedCell value={person.address.zipCode} pattern="#####-###" /></td>
                            <td>{person.address.neighborhood}</td>
                            <td>{person.address.city}</td>
                            <td className={classes.Center}>{person.address.province}</td>
                            <td className={classes.Center}>
                                <Link to={`/person/${person.id}`} 
                                    className="glyphicon glyphicon-edit" >Edit</Link>
                            </td>
                            <td className={classes.Center}>
                                <Link to="/persons" 
                                    className="glyphicon glyphicon-remove" 
                                    onClick={() => this.props.onRemoveHandler(person.id)}>Delete</Link>
                            </td>
                        </tr> 
                    ))
                }
                </tbody>
                <tfoot>
                    <tr>
                        <td colSpan="14">
                            <select value={this.state.perPage} onChange={(event) => this.handlerPerPageSelect(event)}
                                className={classes.PerPages}>
                                <option value="05">05</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                            </select>
                            <Pagination totalPages={this.props.data.totalPages} onClick={this.handlerPagination}/>    
                        </td>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        persons: state.person.persons,
        data: state.person.data
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLoadPersons: (name,identityDocument,email,dateBirth,page,perPage,direction,sortBy) => { 
            dispatch(actions.loadPersons(name,identityDocument,email,dateBirth,page,perPage,direction,sortBy))},
        onEditPerson: (id) => { dispatch(actions.getPerson(id))},
        onRemovePerson: (id) => { dispatch(actions.deletePerson(id))}
    }
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Persons));