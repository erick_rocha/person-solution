import React, { Component } from 'react';
import { Route, withRouter } from 'react-router-dom';
import Layout from './hoc/Layout/Layout';
import Persons from './containers/Persons/Persons';
import Person from './containers/Persons/Person/Person';
import ErrorBoundary from './hoc/ErrorBoundary/ErrorBoundary';

class App extends Component {
  render() {
    return (
      <div className="App">
          <Layout>
          <ErrorBoundary>
            <Route exact path="/" component={Persons} />
            <Route exact path="/person/:id" component={Person} />  
          </ErrorBoundary>
        </Layout>
      </div>
    );
  }
}

export default withRouter(App);
