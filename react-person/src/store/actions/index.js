export {
    loadPersons,
    deletePerson,
    getPerson,
    savePerson
} from './person';

export {
    loadProvinces
} from './resource';