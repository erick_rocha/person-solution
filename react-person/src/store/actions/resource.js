import axios from '../../axios-person';
import * as actionTypes from './actionTypes';

const loadProvincesStart = () => {
    return {
        type: actionTypes.LOAD_PROVINCE_START
    }
}

const loadProvincesSuccess = (provinces) => {
    return {
        type: actionTypes.LOAD_PROVINCE_SUCCESS,
        provinces: provinces
    }
}

const loadProvincesFail = (error) => {
    return {
        type: actionTypes.LOAD_PROVINCE_FAIL,
        error: error
    }
}

export const loadProvinces = () => {
    return dispatch => {
        dispatch(loadProvincesStart());
        axios.get('/api/provinces')
            .then(response => {
                const fields = [];
                response.data.map(p => (fields.push({value: p.acronym, label: p.name})));
                dispatch(loadProvincesSuccess(fields));
            })
            .catch(error =>{
                dispatch(loadProvincesFail(error));
            })
    }
}