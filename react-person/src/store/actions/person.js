import axios from '../../axios-person';
import * as actionTypes from './actionTypes';

const loadPersonsStart = () => {
    return {
        type: actionTypes.LOAD_PERSON_START
    }
}

const loadPersonsSuccess = (data) => {
    const persons = data.content;
    data.content =null;
    return {
        type: actionTypes.LOAD_PERSON_SUCCESS,
        persons: persons,
        data: data
    }
}

const loadPersonsFail = (error) => {
    return {
        type: actionTypes.LOAD_PERSON_FAIL,
        error: error
    }
}

export const loadPersons = (name, identityDocument, email, dateBirth, page, perPage, direction, sortBy) => {
    return dispatch => {
        dispatch(loadPersonsStart());
        const params = `?name=${name}%&identityDocument=${identityDocument}&email=${email}&dateBirth=${dateBirth}`;
        const pagination = `&page=${page}&perPage=${perPage}&direction=${direction}&sortBy=${sortBy}`
        const url = encodeURI(`${params}${pagination}`);
        axios.get(`/api/persons${url}`)
            .then(response => {
                dispatch(loadPersonsSuccess(response.data));
            })
            .catch(error => {
                dispatch(loadPersonsFail(error));
            })
    }
}

const getPersonStart = () => {
    return {
        type: actionTypes.GET_PERSON_START
    }
}

const getPersonSuccess = (person) => {
    return {
        type: actionTypes.GET_PERSON_SUCCESS,
        current: person
    }
}

const getPersonFail = (error) => {
    return {
        type: actionTypes.GET_PERSON_FAIL,
        error: error
    }
}

export const getPerson = (id) => {
    return dispatch => {
        dispatch(getPersonStart())
        axios.get(`/api/persons/${id}`)
            .then(response => {
                dispatch(getPersonSuccess(response.data));
            })
            .catch(error => {
                dispatch(getPersonFail(error));
            })
    }
}

export const deletePerson = (id) => {
    return dispatch => {
        axios.delete(`/api/persons/${id}`)
            .then(response => {

            })
            .catch(error => {

            })
    }
}

export const savePersonStart = () => {
    return {
        type: actionTypes.SAVE_PERSON_START
    }
}

export const savePersonSuccess = () => {
    return {
        type: actionTypes.SAVE_PERSON_SUCCESS
    }
}

export const savePersonFail = (error) => {
    return {
        type: actionTypes.SAVE_PERSON_FAIL,
        error: error
    }
}

export const savePerson = (personData) => {
    return dispatch => {
        dispatch(savePersonStart());
        axios.post(`/api/persons`, personData)
            .then(response => {
                dispatch(savePersonSuccess());
            })
            .catch(error => {
                dispatch(savePersonFail(error));
            })
    }
}