import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    provinces: [],
    loading: false,
    error: null
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.LOAD_PROVINCE_START:
            return updateObject(state,{loading: true, error: null});
        case actionTypes.LOAD_PROVINCE_SUCCESS:
            return updateObject(state,{provinces: action.provinces, error: null, loading: false});
        case actionTypes.LOAD_PROVINCE_FAIL:
            return updateObject(state,{error: action.error });
        default:
            return state;
    }
}

export default reducer;