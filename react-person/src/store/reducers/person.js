import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    persons: [],
    loading: false,
    error: null,
    saved: false,
    current: null,
    data:{
        totalPages: 0
    } 
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.LOAD_PERSON_START:
            return updateObject(state,{loading: true, error: null});
        case actionTypes.LOAD_PERSON_SUCCESS:
            return updateObject(state,{
                persons: action.persons, 
                data: action.data,
                error: null,
                loading: false,
                current: null,
                totalPages:action.data.totalPages
            });
        case actionTypes.LOAD_PERSON_FAIL:
            return updateObject(state,{error: action.error });
        case actionTypes.SAVE_PERSON_SUCCESS:
            return updateObject(state,{saved: true, error: null, loading: false});
        case actionTypes.SAVE_PERSON_FAIL:
            return updateObject(state,{error: action.error });
        case actionTypes.GET_PERSON_START:
            return updateObject(state,{error: null, loading: true});
        case actionTypes.GET_PERSON_SUCCESS:
            return updateObject(state,{current: action.current,error: null, loading: false});
        case actionTypes.GET_PERSON_FAIL:
            return updateObject(state,{error: action.error, loading: false});
        default:
            return state;
    }
}

export default reducer;