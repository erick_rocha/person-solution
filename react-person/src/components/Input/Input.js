import React from 'react';

import classes from './Input.css';

const input = ( props ) => {
    
    return(
        <input className={classes.Input} 
        type={props.type ? props.type : "text"} 
        value={props.value}
        placeholder={props.placeholder} 
        onChange={props.onChange}
        onBlur={props.onBlur}
        onFocus={props.onFocus}/>
)}

export default input;