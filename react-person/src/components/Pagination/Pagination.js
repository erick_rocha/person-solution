import React from 'react';
import classes from './Pagination.css';

const paginator = (props) =>{
    const pages = []
    for (let i = 0; i < props.totalPages;i++){
        pages.push(i);
    }
    return(
        <div className={classes.Pagination}>
            <a>&laquo;</a>
            {pages.map( page => (
                <a key={page} onClick={(event) => props.onClick(event,page)}>{page+1}</a>    
            ))}
            <a>&raquo;</a>
        </div>
    )
} 

export default paginator;