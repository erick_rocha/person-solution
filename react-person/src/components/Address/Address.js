import React from 'react';
import classes from './Address.css';
import layout from '../../shared/layout.css';
import Input from '../../components/Input/Input';
import Select from '../../components/Select/Select';

const address = (props) => (
            <div className={classes.Address}>
                <div className={layout.Row}>
                    <div className={layout.Col_75}>
                        <label>Street</label>
                        <Input value={props.address.street} placeholder="Your Street" />
                    </div>
                    <div className={layout.Col_25}>
                        <label>Number</label>
                        <Input value={props.address.number} placeholder="Your Address Number" />
                    </div>
                </div>
                <div className={layout.Row}>
                    <div className={layout.Col_75}>
                        <label>Complement</label>
                        <Input value={props.address.complement} placeholder="Your complement" />
                    </div>
                    <div className={layout.Col_25}>
                        <label>ZipCode</label>
                        <Input value={props.address.zipCode} placeholder="Your ZipCode" />
                    </div>
                </div>
                <div className={layout.Row}>
                    <div className={layout.Col_33}>
                        <label>Neighborhood</label>
                        <Input value={props.address.neighborhood} placeholder="Your Neighborhood" />
                    </div>
                    <div className={layout.Col_33}>
                        <label>City</label>
                        <Input value={props.address.city} placeholder="Your City" />
                    </div>
                    <div className={layout.Col_33}>
                        <label>Province</label>
                        <Select value={props.address.province} fields={props.fields} />
                    </div>
                </div>
            </div>
        )
     

export default address;