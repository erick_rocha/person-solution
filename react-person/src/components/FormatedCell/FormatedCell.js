import React from "react";
import classes from "./FormatedCell.css";

const formatedCell = props => {
  let formatedValue = "";
  
  if (props.value != null) {
    let value = props.value;
    let pattern = props.pattern;
    let temp = "";
    for (let i = 0; i < value.length; i++) {
      temp = pattern.replace("#", value.substring(i, i + 1));
      pattern = temp;
    }
    formatedValue = temp;
  }
  return <div className={classes.FormatedCell}>{formatedValue}</div>;
};

export default formatedCell;
