import React from 'react';
import logoPath from '../../assets/logo.png';
import classes from './Logo.css';

const logo = (props) => ( 
    <div className={classes.Logo} >
        <img src={logoPath} alt="Person" />
    </div>
)

export default logo;