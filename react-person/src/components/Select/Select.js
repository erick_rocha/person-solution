import React from 'react';
import classes from './Select.css';

const select = (props) => (
    <select className={classes.Select} value={props.select} onChange={props.onChange}>
        {props.fields.map(field => (
            <option key={field.value} value={field.value}>{field.label}</option>
        ))}
    </select>
)

export default select;