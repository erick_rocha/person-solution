import React from 'react';
import { NavLink } from 'react-router-dom';
import Logo from '../Logo/Logo';
import classes from './Toolbar.css';


const toolbar = (props) => (
    <header className={classes.Toolbar}>
        <Logo />
        <div className={classes.Title}>
            <NavLink to="/">PERSONS</NavLink>
        </div>
        <nav>
            
        </nav>
    </header>
)

export default toolbar;