import React from 'react';

import classes from './Button.css';

const button = (props) => (
    <button
        disabled={props.disabled}
        className={[classes.Button, classes[props.position],classes[props.type]].join(' ')}
        onClick={props.onClick}>{props.children}</button>
);

export default button;