import React, { Component } from 'react';
import classes from './Calendar.css';

class Calendar extends Component {
    render() {
        return (
            <div className={classes.Calendar}>
                <div className={classes.Month} >
                    <ul>
                        <li className={classes.Prev}><a>&#10094;</a></li>
                        <li className={classes.Next}>&#10095;</li>
                        <li>August<br/><span className={classes.Year}>2017</span></li>
                    </ul>
                </div>
            </div>
        );
    }

}

export default Calendar;